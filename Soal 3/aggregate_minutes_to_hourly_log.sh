#!/bin/bash

MemTotal=0
MemUsed=0
MemFree=0
MemShared=0
MemBuff=0
MemAvailable=0
SwapTotal=0
SwapUsed=0
SwapFree=0
PathUsed=0
PathSize=0

MaxMemTotal=-1
MaxMemUsed=-1
MaxMemFree=-1
MaxMemShared=-1
MaxMemBuff=-1
MaxMemAvailable=-1
MaxSwapTotal=-1
MaxSwapUsed=-1
MaxSwapFree=-1
MaxPathSize=-1

MinMemTotal=1000000000000
MinMemUsed=1000000000000
MinMemFree=1000000000000
MinMemShared=1000000000000
MinMemBuff=1000000000000
MinMemAvailable=1000000000000
MinSwapTotal=1000000000000
MinSwapUsed=1000000000000
MinSwapFree=1000000000000
MinPathSize=1000000000000

MyUser=$(whoami)
MyFolder="/home/${MyUser}"
#printf "$MyFolder\n"
MyTime=$(date +%Y%m%d%H)
MyFile="${MyFolder}/log/metrics_${MyTime}*.log"
MyTarget="${MyFolder}/log/metrics_agg_${MyTime}.log"
#printf "$MyFile\n"

FileList=$(ls -1 ${MyFile})
#printf "$FileList\n"
IFS=$'\n' read -d '' -r -a farr <<< "$FileList"
TotalFile=${#farr[@]}
if [ $TotalFile -lt 1 ]; then
        TotalFile=1
fi
for fname in ${farr[@]}; do
    fc=$(cat $fname)
    IFS=$'\n' read -d '' -r -a fcarr <<< "$fc"
    IFS=$',' read -d '' -r -a varr <<< "${fcarr[1]}"
    #echo ${varr[@]}
    mt="${varr[0]//$'\n'/}"
    mu="${varr[1]//$'\n'/}"
    mf="${varr[2]//$'\n'/}"
    ms="${varr[3]//$'\n'/}"
    mb="${varr[4]//$'\n'/}"
    ma="${varr[5]//$'\n'/}"
    st="${varr[6]//$'\n'/}"
    su="${varr[7]//$'\n'/}"
    sf="${varr[8]//$'\n'/}"
    pu="${varr[9]//$'\n'/}"
    ps="${varr[10]//$'\n'/}"
    if [ $mt -gt $MaxMemTotal ]; then
        MaxMemTotal=$mt
    fi
    if [ $mt -lt $MinMemTotal ]; then
        MinMemTotal=$mt
    fi
    MemTotal=$(( MemTotal + mt ))
    if [ $mu -gt $MaxMemUsed ]; then
        MaxMemUsed=$mu
    fi
    if [ $mu -lt $MinMemUsed ]; then
        MinMemUsed=$mu
    fi
    MemUsed=$(( MemUsed + mu ))
    if [ $mf -gt $MaxMemFree ]; then
        MaxMemFree=$mf
    fi
    if [ $mf -lt $MinMemFree ]; then
        MinMemFree=$mf
    fi
    MemFree=$(( MemFree + mf ))
    if [ $ms -gt $MaxMemShared ]; then
        MaxMemShared=$ms
    fi
    if [ $ms -lt $MinMemShared ]; then
        MinMemShared=$ms
    fi
    MemShared=$(( MemShared + ms ))
    if [ $mb -gt $MaxMemBuff ]; then
        MaxMemBuff=$mb
    fi
    if [ $mb -lt $MinMemBuff ]; then
        MinMemBuff=$mb
    fi
    MemBuff=$(( MemBuff + mb ))
    if [ $ma -gt $MaxMemAvailable ]; then
        MaxMemAvailable=$ma
    fi
    if [ $ma -lt $MinMemAvailable ]; then
        MinMemAvailable=$ma
    fi
    MemAvailable=$(( MemAvailable + ma ))
    if [ $st -gt $MaxSwapTotal ]; then
        MaxSwapTotal=$st
    fi
    if [ $st -lt $MinSwapTotal ]; then
        MinSwapTotal=$st
    fi
    SwapTotal=$(( SwapTotal + st ))
    if [ $su -gt $MaxSwapUsed ]; then
        MaxSwapUsed=$su
    fi
    if [ $su -lt $MinSwapUsed ]; then
        MinSwapUsed=$su
    fi
    SwabUsed=$(( SwapUsed + su ))
    if [ $sf -gt $MaxSwapFree ]; then
        MaxSwapFree=$sf
    fi
    if [ $sf -lt $MinSwapFree ]; then
        MinSwapFree=$sf
    fi
    SwapFree=$(( SwapFree + sf ))
    PathUsed="$pu"
    qu=${ps: -1}
    psl=${#ps}
    nu=${ps::($psl-1)}
    if [ "$qu" == "K" ]; then
        nu=$(( nu*1024 ))
    elif [ "$qu" == "M" ]; then
        nu=$(( nu*1024*1024 ))
    elif [ "$qu" == "G" ]; then
        nu=$(( nu*1024*1024*1024 ))
    fi
    if [ $nu -gt $MaxPathSize ]; then
        MaxPathSize=$nu
    fi
    if [ $nu -lt $MinPathSize ]; then
        MinPathSize=$nu
    fi
    PathSize=$(( PathSize + nu ))
done

qumax=" "
numax=$MaxPathSize
psl=${#numax}
if [ $psl -gt 3 ]; then
        numax=$(( numax/1024 ))
        qumax="K"
fi
if [ $psl -gt 6 ]; then
        numax=$(( numax/1024 ))
        qumax="M"
fi
if [ $psl -gt "9" ]; then
        numax=$(( numax/1024 ))
        qumax="G"
fi

qumin=" "
numin=$MinPathSize
psl=${#numin}
if [ $psl -gt 3 ]; then
        numin=$(( numin/1024 ))
        qumin="K"
fi
if [ $psl -gt 6 ]; then
        numin=$(( numin/1024 ))
        qumin="M"
fi
if [ $psl -gt "9" ]; then
        numin=$(( numin/1024 ))
        qumin="G"
fi


qu=" "
nu=$(( PathSize/TotalFile ))
psl=${#nu}
if [ $psl -gt 3 ]; then
        nu=$(( nu/1024 ))
        qu="K"
fi
if [ $psl -gt 6 ]; then
        nu=$(( nu/1024 ))
        qu="M"
fi
if [ $psl -gt "9" ]; then
        nu=$(( nu/1024 ))
        qu="G"
fi

MemTotal=$(( MemTotal/TotalFile ))
MemUsed=$(( MemUsed/TotalFile ))
MemFree=$(( MemFree/TotalFile ))
MemShared=$(( MemShared/TotalFile ))
MemBuff=$(( MemBuff/TotalFile ))
MemAvailable=$(( MemAvailable/TotalFile ))
SwapTotal=$(( SwapTotal/TotalFile ))
SwapUsed=$(( SwapUsed/TotalFile ))
SwapFree=$(( SwapFree/TotalFile ))

echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $MyTarget
echo "minimum,${MinMemTotal},${MinMemUsed},${MinMemFree},${MinMemShared},${MinMemBuff},${MinMemAvailable},${MinSwapTotal},${MinSwapUsed},${MinSwapFree},${PathUsed},${numin}${qumin}" >> $MyTarget
echo "maximum,${MaxMemTotal},${MaxMemUsed},${MaxMemFree},${MaxMemShared},${MaxMemBuff},${MaxMemAvailable},${MaxSwapTotal},${MaxSwapUsed},${MaxSwapFree},${PathUsed},${numax}${qumax}" >> $MyTarget
echo "average,${MemTotal},${MemUsed},${MemFree},${MemShared},${MemBuff},${MemAvailable},${SwapTotal},${SwapUsed},${SwapFree},${PathUsed},${nu}${qu}" >> $MyTarget
chmod 700 ${MyTarget}
