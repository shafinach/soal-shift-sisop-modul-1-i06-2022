#!/bin/bash
MyUser=$(whoami)
MyFolder="/home/${MyUser}"
#printf "$MyFolder\n"
MyTime=$(date +%Y%m%d%H%m%S)
MyFile="${MyFolder}/log/metrics_${MyTime}.log"
#printf"$MyFile\n"

FreeMem=$(free -m)
IFS=$'\n' read -d '' -r -a fmarr <<< "$FreeMem"
IFS=$' ' read -d '' -r -a memarr <<< "${fmarr[1]}"
IFS=$' ' read -d '' -r -a swaparr <<< "${fmarr[2]}"
MemTotal="${memarr[1]//$'\n'/}"
MemUsed="${memarr[2]//$'\n'/}"
MemFree="${memarr[3]//$'\n'/}"
MemShared="${memarr[4]//$'\n'/}"
MemBuff="${memarr[5]//$'\n'/}"
MemAvailable="${memarr[6]//$'\n'/}"
SwapTotal="${swaparr[1]//$'\n'/}"
SwapUsed="${swaparr[2]//$'\n'/}"
SwapFree="${swaparr[3]//$'\n'/}"

DiskUsed=$(du -sh ${MyFolder})
IFS=$'\t' read -d '' -r -a duarr <<< "$DiskUsed"
PathUsed="${duarr[1]//$'\n'/}"
PathFree="${duarr[0]//$'\n'/}"

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> ${MyFile}
echo "${MemTotal},${MemUsed},${MemFree},${MemShared},${MemBuff},${MemAvailable},${SwapTotal},${SwapUsed},${SwapFree},${PathUsed},${PathFree}" >> ${MyFile}

chmod 700 ${MyFile}
