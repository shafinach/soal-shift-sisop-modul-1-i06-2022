#!/bin/bash

mkdir -p ./users/user.txt

echo "To make user, there are some requirement for the password"
echo "1. Minimum 8 characters"
echo "2. Have at least 1 capital letter and 1 lowercase letter"
echo "3. Alphanumeric"
echo "4. Cannot be the same as username"

echo "Username: "
read user;

bool=1
while [ $bool -eq 1 ]
do
echo "Password: "
read pass;
if [ ${#pass} -lt 8 ]
then
	echo "Please add minimmum 8 characters"
elif ! [[ "$pass" =~ [A-Z] ]] && ! [[ "$pass" =~ [a-z] ]];
then
	echo "Have at least 1 capital letter and lowercase letter"
elif [[ "$pass" =~ [^a-zA-Z0-9] ]];
then
	echo "Please input alphanumeric"
elif [[ "$pass" == "$user" ]];
then
	echo "The password cannot be the same as username"
else
bool=0
fi
done

direct="users/user.txt"
date=$(date '+$m/$d/%y %H:%M:%S')

if grep -qF "$user" $direct
then 
	echo "ERROR: User already exist" 
	echo $date "REGISTER:ERROR user already exist" >>log.txt
else
	echo "Username registered successfully"
	echo $date "REGISTER:INFO User $user success" >>log.txt
	echo "USER : $user PASS : $pass" >>register.txt
fi


