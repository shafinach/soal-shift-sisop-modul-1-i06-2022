#!/bin/bash

zip_folder() {
	zip --pass $pass -r $folder.zip $folder
	rm -rf $folder
}

unzip_folder() {
	unzip -P $pass $folder.zip
}

download_image() {
	for ((i=1; i<=n; i++))
	do
	if [ $i -le 9]
	then
	num="0"
	else
	num=""
	fi

	wget -O $folder/PIC_$i.jpg https://loremflickr.com/320/240
	done
	zip_folder
}

echo "Username: "
read user;
echo "Password: "
read pass;

direct="users/user.txt"

if grep -qF "$pass" $direct
then
echo "LOGIN: INFO User $user logged in" >>log.txt


