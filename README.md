# soal-shift-sisop-modul-1-I06-2022

I06 Sisop Group 2022

- Yusuf Faiz Shalahudin (5025201119)
- Shafina Chaerunisa (5025201129)
- Bagus Nugraha (5025201162)

## Soal 1

Han creates a register system in the register.sh script and every user who is successfully registered is stored in the ./users/user.txt file.

By this, i use:

```
mkdir -p ./users/user.txt
```
Mkdir is used to make a new directory and direct register.sh file into user.txt

After that, there are some requirements to login as user. 
```
if [ ${#pass} -lt 8 ]
// requirement to add minimum 8 characters
elif ! [[ "$pass" =~ [A-Z] ]] && ! [[ "$pass" =~ [a-z] ]];
// add at least 1 capital letter and lowercase letter
elif [[ "$pass" =~ [^a-zA-Z0-9] ]];
// requirement to input alphanumeric"
elif [[ "$pass" == "$user" ]];
// requirement for password cannot be the same as username"
```

To input username and password, we're using read
```
read -p "Username: "
read -s -p "Password: "
```

In C, we were asked to have a format in log.txt with date and hour
```
date=$(date '+%m/%d/%y %H:%M:%S')
```
In log.txt there will be a message in every user actions

1. When trying to register with a registered username, the message in the log is REGISTER: ERROR User already exists
2. When the registration attempt is successful, the message in the log is REGISTER: INFO User USERNAME registered successfully

```
if grep -qF "$user" $direct
then 
	echo "ERROR: User already exist" 
	echo $date "REGISTER:ERROR user already exist" >>log.txt
else
	echo "Username registered successfully"
	echo $date "REGISTER:INFO User $user success" >>log.txt
	echo "USER : $user PASS : $pass" >>register.txt
fi
```

In main.sh 
Every images that are already downloaded will be save or move into zip folder with password on it

```
zip_folder() {
	zip --pass $pass -r $folder.zip $folder
	rm -rf $folder
}
```

There's also a unzip file. If there is already a zip file with the same name, then unzip the existing zip file first, then start adding new images, then the folder is zipped again with the password according to the user.

```
unzip_folder() {
	unzip -P $pass $folder.zip
}
```

To download the image, we have to name the file as requirement using "PIC_XX" for example "PIC_04" or "PIC_11". To give a zero number for unit number we use loop. Afterward, we use wget to retrieve the download images and put it in zip folder

```
for ((i=1; i<=n; i++))
	do
	if [ $i -le 9]
	then
	num="0"
	else
	num=""
	fi

	wget -O $folder/PIC_$i.jpg https://loremflickr.com/320/240
	done
	zip_folder
```

This is the best we can do in main.sh file, we haven't try to logged in and try "n" downloaded images

## Soal 2

In Question a, Dapos should create a folder with "forensic_log_website_daffainfo_log" name. With this task type, i use the command
```
[ -d forensic_log_website_daffainfo_log ] && echo "Directory Exists" || mkdir forensic_log_website_daffainfo_log
```
The explanation : 
- -d directory is to check if the directory is exist or not with addition of double and(&&) which means logic if the directory exists, then echo the designated message
- After that,add logic with || with command mkdir directory to create the directory with designated name immediately


There is task which tell us to make command to find average requests per hour. I actually didn't get how to processed it to get the average per hour with my logic. so i haven't create any code/command for it.


After Creating the folder, Dapos want to know which IP that send most requests to the server and how many the same ip send the requests.

In this case, i use :

```
cat log_website_daffainfo.log | awk -F ':' '{print $1}' | uniq -c | sort -nr | head -n 1 >> forensic_log_website_daffainfo_log/result.txt
```

The Explanation :
- cat file_name to read the file
- the usage of || characters to allow us to use many command at the same time
- awk -F ':' '{print $1}' command awk to stop the printing at the separator then we print only the first column which in this case displays IP
- uniq -c is command to count every request that has the same ip
- sort -nr is used to print the IP reversed which display us the ip that sends most request from the biggest to the lowest
- head -n 1 is a command that used to print the first line. in this case, it will print only the ip that sends most of the requests.
- >> file_name is used to put the output in designated file.

Eventhough i have create the awk in subtask c, I still cannot create how to print the output as the demands in the task.

After knowing the task, Dapos wants to know how many requests that uses curl as their user agents.The command to find how many request that is :

```
awk -v str='curl' '{c += gsub(str, str) } END {printf "\n ada %s request yang menggunakan curl sebagai user agent \n\n",c}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
```

The Explanation for this code :

- I choose the -v str ='curl' to find the column that has curl agent in it.
- c+= gsub(str,str) is a command that i use to replace every column that has curl in it and replace it with the same str which is curl too and then count how many replacing process that happens for indicating how many requester that use curl as happy agent
-after that i put printf to print the output as per the request in the question that contains how many request that use curl
- >> file_name is used to put the output in designated file.

The last task for question number 2  is to find ip that send requests to the web at 23 january 02:00 AM, in this task i use:

```
cat log_website_daffainfo.log | awk '/22\/Jan\/2022:02/ {  split($1,a,":")
        c[a[1]]++;
        count++}
        END{
        for(i in c){
        print i "IP Address Jam 2 Pagi"}
} ' >> forensic_log_website_daffainfo_log/result.txt

```

The explanation for this code is :
- cat file_name to read the file.
- /23\/Jan\/2022:02/ is used in the awk to find the ip at the date that match the requirements in the log.
-  split($1,a,":") which command to split the first column of the log, put the first column that has been processed to as a variable and the separator limit is ":".
- Then we count and put ip so that we dont have same ip in the output as how many request with same ip in the same hour
- After that we use for to print the IP 
-  >> file_name is used to put the output in designated file.


## Soal 3


In the Question it is asked to record the memory of Ubay's Laptop as such I created a script to do so.
As usual create the file using #!/bin/bash, then declare MyUser as the user

```
#!/bin/bash
MyUser=$(whoami)

MyFolder="/home/${MyUser}" "for the path directory"

#printf "$MyFolder\n"
"print the path"
MyTime=$(date +%Y%m%d%H%m%S)
Create the date
MyFile="${MyFolder}/log/metrics_${MyTime}.log"
the outpur of the metrics on Question a.
#printf "$MyFile\n"
print it
Below is the function to free the ram and assign the memory that came out from the output
FreeMem=$(free -m) 
IFS=$'\n' read -d '' -r -a fmarr <<< "$FreeMem"
IFS=$' ' read -d '' -r -a memarr <<< "${fmarr[1]}"
IFS=$' ' read -d '' -r -a swaparr <<< "${fmarr[2]}"

assign each memory to a variable
MemTotal="${memarr[1]//$'\n'/}"
MemUsed="${memarr[2]//$'\n'/}"
MemFree="${memarr[3]//$'\n'/}"
MemShared="${memarr[4]//$'\n'/}"
MemBuff="${memarr[5]//$'\n'/}"
MemAvailable="${memarr[6]//$'\n'/}"
SwapTotal="${swaparr[1]//$'\n'/}"
SwapUsed="${swaparr[2]//$'\n'/}"
SwapFree="${swaparr[3]//$'\n'/}"

read the Disk and assign the path used and path that if free to a variable

DiskUsed=$(du -sh ${MyFolder})
IFS=$'\t' read -d '' -r -a duarr <<< "$DiskUsed"
PathUsed="${duarr[1]//$'\n'/}"
PathFree="${duarr[0]//$'\n'/}"

Printout the metrics for this occation 

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> ${MyFile}
echo "${MemTotal},${MemUsed},${MemFree},${MemShared},${MemBuff},${MemAvailable},${SwapTotal},${SwapUsed},${SwapFree},${PathUsed},${PathFree}" >> ${MyFile}

chmod 700 ${MyFile} change the mode of the file so the user can read and write

(crontab)
*****/path/to/your/script
```
^^ minute_log.sh file 



for the next file is the aggregate_minutes_to_hourly_log.sh
it is tasked to aggregate the file log to hourly and put our the average, maximum, and minimum of every metrics.

create the file
#!/bin/bash

asssign variables for each memory
```
MemTotal=0
MemUsed=0
MemFree=0
MemShared=0
MemBuff=0
MemAvailable=0
SwapTotal=0
SwapUsed=0
SwapFree=0
PathUsed=0
PathSize=0

MaxMemTotal=-1
MaxMemUsed=-1
MaxMemFree=-1
MaxMemShared=-1
MaxMemBuff=-1
MaxMemAvailable=-1
MaxSwapTotal=-1
MaxSwapUsed=-1
MaxSwapFree=-1
MaxPathSize=-1

MinMemTotal=1000000000000
MinMemUsed=1000000000000
MinMemFree=1000000000000
MinMemShared=1000000000000
MinMemBuff=1000000000000
MinMemAvailable=1000000000000
MinSwapTotal=1000000000000
MinSwapUsed=1000000000000
MinSwapFree=1000000000000
MinPathSize=1000000000000
```

to assign variables to the user currently
```
MyUser=$(whoami)
MyFolder="/home/${MyUser}"
#printf "$MyFolder\n"
MyTime=$(date +%Y%m%d%H)
MyFile="${MyFolder}/log/metrics_${MyTime}*.log"
MyTarget="${MyFolder}/log/metrics_agg_${MyTime}.log"
#printf "$MyFile\n"
```
check the list of files to check
```
FileList=$(ls -1 ${MyFile})
#printf "$FileList\n"
IFS=$'\n' read -d '' -r -a farr <<< "$FileList"
TotalFile=${#farr[@]}
if [ $TotalFile -lt 1 ]; then
        TotalFile=1




fi
for fname in ${farr[@]}; do
    fc=$(cat $fname)
    IFS=$'\n' read -d '' -r -a fcarr <<< "$fc"
    IFS=$',' read -d '' -r -a varr <<< "${fcarr[1]}"
    #echo ${varr[@]}
    mt="${varr[0]//$'\n'/}"
    mu="${varr[1]//$'\n'/}"
    mf="${varr[2]//$'\n'/}"
    ms="${varr[3]//$'\n'/}"
    mb="${varr[4]//$'\n'/}"
    ma="${varr[5]//$'\n'/}"
    st="${varr[6]//$'\n'/}"
    su="${varr[7]//$'\n'/}"
    sf="${varr[8]//$'\n'/}"
    pu="${varr[9]//$'\n'/}"
    ps="${varr[10]//$'\n'/}"
    if [ $mt -gt $MaxMemTotal ]; then
        MaxMemTotal=$mt
    fi
    if [ $mt -lt $MinMemTotal ]; then
        MinMemTotal=$mt
    fi
    MemTotal=$(( MemTotal + mt ))
    if [ $mu -gt $MaxMemUsed ]; then
        MaxMemUsed=$mu
    fi
    if [ $mu -lt $MinMemUsed ]; then
        MinMemUsed=$mu
    fi
    MemUsed=$(( MemUsed + mu ))
    if [ $mf -gt $MaxMemFree ]; then
        MaxMemFree=$mf
    fi
    if [ $mf -lt $MinMemFree ]; then
        MinMemFree=$mf
    fi
    MemFree=$(( MemFree + mf ))
    if [ $ms -gt $MaxMemShared ]; then
        MaxMemShared=$ms
    fi
    if [ $ms -lt $MinMemShared ]; then
        MinMemShared=$ms
    fi
    MemShared=$(( MemShared + ms ))
    if [ $mb -gt $MaxMemBuff ]; then
        MaxMemBuff=$mb
    fi
    if [ $mb -lt $MinMemBuff ]; then
        MinMemBuff=$mb
    fi
    MemBuff=$(( MemBuff + mb ))
    if [ $ma -gt $MaxMemAvailable ]; then
        MaxMemAvailable=$ma
    fi
    if [ $ma -lt $MinMemAvailable ]; then
        MinMemAvailable=$ma
    fi
    MemAvailable=$(( MemAvailable + ma ))
    if [ $st -gt $MaxSwapTotal ]; then
        MaxSwapTotal=$st
    fi
    if [ $st -lt $MinSwapTotal ]; then
        MinSwapTotal=$st
    fi
    SwapTotal=$(( SwapTotal + st ))
    if [ $su -gt $MaxSwapUsed ]; then
        MaxSwapUsed=$su
    fi
    if [ $su -lt $MinSwapUsed ]; then
        MinSwapUsed=$su
    fi
    SwabUsed=$(( SwapUsed + su ))
    if [ $sf -gt $MaxSwapFree ]; then
        MaxSwapFree=$sf
    fi
    if [ $sf -lt $MinSwapFree ]; then
        MinSwapFree=$sf
    fi
    SwapFree=$(( SwapFree + sf ))
    PathUsed="$pu"
    qu=${ps: -1}
    psl=${#ps}
    nu=${ps::($psl-1)}
    if [ "$qu" == "K" ]; then
        nu=$(( nu*1024 ))
    elif [ "$qu" == "M" ]; then
        nu=$(( nu*1024*1024 ))
    elif [ "$qu" == "G" ]; then
        nu=$(( nu*1024*1024*1024 ))
    fi
    if [ $nu -gt $MaxPathSize ]; then
        MaxPathSize=$nu
    fi
    if [ $nu -lt $MinPathSize ]; then
        MinPathSize=$nu
    fi
    PathSize=$(( PathSize + nu ))
done
for the maximum of the metrics

qumax=" "
numax=$MaxPathSize
psl=${#numax}
if [ $psl -gt 3 ]; then
        numax=$(( numax/1024 ))
        qumax="K"
fi
if [ $psl -gt 6 ]; then
        numax=$(( numax/1024 ))
        qumax="M"
fi
if [ $psl -gt "9" ]; then
        numax=$(( numax/1024 ))
        qumax="G"
fi

qumin=" "
numin=$MinPathSize
psl=${#numin}
if [ $psl -gt 3 ]; then
        numin=$(( numin/1024 ))
        qumin="K"
fi
if [ $psl -gt 6 ]; then
        numin=$(( numin/1024 ))
        qumin="M"
fi
if [ $psl -gt "9" ]; then
        numin=$(( numin/1024 ))
        qumin="G"
fi


qu=" "
nu=$(( PathSize/TotalFile ))
psl=${#nu}
if [ $psl -gt 3 ]; then
        nu=$(( nu/1024 ))
        qu="K"
fi
if [ $psl -gt 6 ]; then
        nu=$(( nu/1024 ))
        qu="M"
fi
if [ $psl -gt "9" ]; then
        nu=$(( nu/1024 ))
        qu="G"
fi
```
assign for the average
```
MemTotal=$(( MemTotal/TotalFile ))
MemUsed=$(( MemUsed/TotalFile ))
MemFree=$(( MemFree/TotalFile ))
MemShared=$(( MemShared/TotalFile ))
MemBuff=$(( MemBuff/TotalFile ))
MemAvailable=$(( MemAvailable/TotalFile ))
SwapTotal=$(( SwapTotal/TotalFile ))
SwapUsed=$(( SwapUsed/TotalFile ))
SwapFree=$(( SwapFree/TotalFile ))
```

output of the file

```
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $MyTarget
echo "minimum,${MinMemTotal},${MinMemUsed},${MinMemFree},${MinMemShared},${MinMemBuff},${MinMemAvailable},${MinSwapTotal},${MinSwapUsed},${MinSwapFree},${PathUsed},${numin}${qumin}" >> $MyTarget
echo "maximum,${MaxMemTotal},${MaxMemUsed},${MaxMemFree},${MaxMemShared},${MaxMemBuff},${MaxMemAvailable},${MaxSwapTotal},${MaxSwapUsed},${MaxSwapFree},${PathUsed},${numax}${qumax}" >> $MyTarget
echo "average,${MemTotal},${MemUsed},${MemFree},${MemShared},${MemBuff},${MemAvailable},${SwapTotal},${SwapUsed},${SwapFree},${PathUsed},${nu}${qu}" >> $MyTarget
chmod 700 ${MyTarget}
```
